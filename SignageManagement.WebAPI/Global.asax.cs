﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using SignageManagement.Data;
using SignageManagement.WebAPI.Helpers;
using WebMatrix.WebData;

namespace SignageManagement.WebAPI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<SignageManagementContext>());
            var db = new SignageManagementContext();
            db.Database.Initialize(true);

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            WebSecurity.InitializeDatabaseConnection("SignageManagementContext", "UserProfiles", "Id", "Username", autoCreateTables: false);

            if (!Roles.RoleExists(Constants.Administrator))
            {
                Roles.CreateRole(Constants.Administrator);
            }

            if (!Roles.RoleExists(Constants.Global))
            {
                Roles.CreateRole(Constants.Global);
            }

            if (!Roles.RoleExists(Constants.Local))
            {
                Roles.CreateRole(Constants.Local);
            }

            if (!WebSecurity.UserExists("admin"))
            {
                var now = DateTime.UtcNow;
                WebSecurity.CreateUserAndAccount("admin", "password",
                    new
                    {
                        FirstName = "Harish",
                        LastName = "Panamgipalli",
                        Email = "harish@thoughtstudio.com.au",
                        Created = now,
                        Modified = now
                    });
            }
            
            if (!Roles.GetRolesForUser("admin").Contains("Administrator"))
            {
                Roles.AddUsersToRoles(new[] { "admin" }, new[] { "Administrator" });
            }
        }
    }
}