﻿using System;

namespace SignageManagement.WebAPI.Helpers
{
    public static class AppTimezone
    {
        public static TimeZoneInfo MyTimeZone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
        
        //method to get current datetime in UTC
        public static DateTime GetCurrentTime()
        {
            var currentDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, MyTimeZone);
            return currentDateTime;
        }

        //method to get local time from utc time
        public static DateTime GetTime(DateTime datetime)
        {
            var convertedDate = DateTime.SpecifyKind(datetime, DateTimeKind.Utc);
            var reqDateTime = TimeZoneInfo.ConvertTimeFromUtc(convertedDate, MyTimeZone);
            return reqDateTime;
        }

        //method to get utc time from local time
        public static DateTime GetUtcTime(DateTime datetime)
        {
            var reqDateTime = TimeZoneInfo.ConvertTimeToUtc(datetime, MyTimeZone);
            return reqDateTime;
        }
    }
}