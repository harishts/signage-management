﻿namespace SignageManagement.WebAPI.Helpers
{
    public static class Constants
    {
        public const string Administrator = "Administrator";
        public const string Global = "Global";
        public const string Local = "Local";
    }
}