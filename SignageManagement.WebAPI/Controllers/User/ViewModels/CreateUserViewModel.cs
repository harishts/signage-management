﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SignageManagement.WebAPI.Helpers;

namespace SignageManagement.WebAPI.Controllers.User.ViewModels
{
    public class CreateUserViewModel
    {
        public CreateUserViewModel()
        {
            Populate();
        }

        public void Populate()
        {
            // TODO: should i use Roles.GetAllRoles();?
            var roles = new List<string>
            {
                Constants.Global,
                Constants.Local,
                Constants.Administrator
            };
            RoleSelectList = new SelectList(roles);
        }


        [Required]
        public string Username { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Mobile { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DisplayName("Role")]
        public string Role { get; set; }
        public SelectList RoleSelectList { get; set; }

    }
}