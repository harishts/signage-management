﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SignageManagement.Data.Models;
using SignageManagement.WebAPI.Helpers;

namespace SignageManagement.WebAPI.Controllers.User.ViewModels
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {
            
        }

        public EditUserViewModel(UserProfile user, string role)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Mobile = user.Mobile;
            Role = role;

            Populate();
        }

        public void Populate()
        {
            // TODO: should i use Roles.GetAllRoles();?
            var roles = new List<string>
            {
                Constants.Global,
                Constants.Local,
                Constants.Administrator
            };
            RoleSelectList = new SelectList(roles);
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Mobile { get; set; }

        [Required]
        [DisplayName("Role")]
        public string Role { get; set; }
        public SelectList RoleSelectList { get; set; }

       
    }
}