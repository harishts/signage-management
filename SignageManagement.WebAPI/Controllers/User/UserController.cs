﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using SignageManagement.Data;
using SignageManagement.WebAPI.Controllers.User.ViewModels;
using SignageManagement.WebAPI.Helpers;
using WebMatrix.WebData;

namespace SignageManagement.WebAPI.Controllers.User
{
    [Authorize(Roles = Constants.Administrator)]
    public class UserController : Controller
    {
        private SignageManagementContext db = new SignageManagementContext();
        //
        // GET: /User/

        public ActionResult Index()
        {
            var users = db.UserProfiles;
            return View(users);
        }

        public ActionResult Create()
        {
            using (var context = new SignageManagementContext())
            {
                // TODO: use roles from DB
                var allRoles = context.Roles.ToList();
                var viewModel = new CreateUserViewModel();

                return View(viewModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var now = DateTime.UtcNow;
                    WebSecurity.CreateUserAndAccount(viewModel.Username, viewModel.Password, new
                    {
                        viewModel.FirstName,
                        viewModel.LastName,
                        viewModel.Email,
                        viewModel.Mobile,
                        Created = now,
                        Modified = now
                    });
                    Roles.AddUsersToRoles(new[] { viewModel.Username }, new[] { viewModel.Role });
                    using (var context = new SignageManagementContext())
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            using (var context = new SignageManagementContext())
            {
                var allRoles = context.Roles.ToList();

                viewModel.Populate();

                return View(viewModel);
            }
        }

        public ActionResult Edit(int id)
        {
            using (var context = new SignageManagementContext())
            {
                var user = context.UserProfiles.Single(u => u.Id == id);
                // TODO: admin
                if (user.Username == "admin")
                {
                    if (User.Identity.Name != "admin")
                    {
                        return HttpNotFound();
                    }
                }

                var allRoles = context.Roles.ToList();

                var viewModel = new EditUserViewModel(user, Roles.GetRolesForUser(user.Username).First());

                return View(viewModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    using (var context = new SignageManagementContext())
                    {
                        var user = context.UserProfiles.Single(u => u.Id == viewModel.Id);

                        // TODO: admin
                        if (user.Username == "admin")
                        {
                            if (User.Identity.Name != "admin")
                            {
                                return HttpNotFound();
                            }
                        }

                        user.FirstName = viewModel.FirstName;
                        user.LastName = viewModel.LastName;
                        user.Email = viewModel.Email;
                        user.Mobile = viewModel.Mobile;
                        user.Modified = DateTime.UtcNow;


                        if (!Roles.IsUserInRole(user.Username, viewModel.Role))
                        {
                            Roles.RemoveUserFromRoles(user.Username, Roles.GetRolesForUser(user.Username));
                            Roles.AddUsersToRoles(new[] { user.Username }, new[] { viewModel.Role });
                        }
                        
                        context.Entry(user).State = EntityState.Modified;
                        context.SaveChanges();

                        return RedirectToAction("Index");
                    }

                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            using (var context = new SignageManagementContext())
            {
                var allRoles = context.Roles.ToList();

                viewModel.Populate();

                return View(viewModel);
            }
        }




        public ActionResult ResetPassword(int id)
        {
            using (var context = new SignageManagementContext())
            {
                var user = context.UserProfiles.Single(u => u.Id == id);

                // TODO: admin
                if (user.Username == "admin")
                {
                    if (User.Identity.Name != "admin")
                    {
                        return HttpNotFound();
                    }
                }

                var viewModel = new ResetPasswordViewModel
                {
                    UserId = user.Id,
                    Username = user.Username
                };

                return View(viewModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // TODO: admin
                if (viewModel.Username == "admin")
                {
                    if (User.Identity.Name != "admin")
                    {
                        return HttpNotFound();
                    }
                }

                try
                {
                    WebSecurity.GeneratePasswordResetToken(viewModel.Username);
                    using (var context = new SignageManagementContext())
                    {
                        var user = context.Membership.Single(u => u.UserId == viewModel.UserId);
                        WebSecurity.ResetPassword(user.PasswordVerificationToken, viewModel.Password);
                    }
                }
                catch (Exception)
                {
                }
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }


        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel viewModel, string returnUrl)
        {
            // TODO: remember me
            if (ModelState.IsValid && WebSecurity.Login(viewModel.Username, viewModel.Password, true ))
            {
                // TODO: why was i gettin ghtat error
                return RedirectToAction("Index", "Customer");
                //return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(viewModel);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login");
        }





        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

     
        public ActionResult Delete(int id)
        {
            using (var context = new SignageManagementContext())
            {
                try
                {
                    var user = context.UserProfiles.Find(id);

                    context.Entry(user).State = EntityState.Deleted;

                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    return Json(false);
                }
            }
            return Json(true);
        }
        
    }
}
