﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SignageManagement.Data;
using SignageManagement.Data.Models;

namespace SignageManagement.WebAPI.Controllers
{
    public class CustomersController : ApiController
    {
        private readonly SignageManagementContext _db = new SignageManagementContext();

        // GET api/customers
        [HttpGet]
        [ActionName("GetCustomers")]
        public IEnumerable<Customer> GetCustomers()
        {
            return _db.Customers.AsEnumerable();
        }

        // GET api/customers/5
        [HttpGet]
        [ActionName("GetCustomer")]
        public Customer GetCustomer(int id)
        {
            var customer = _db.Customers.Find(id);
            if (customer == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return customer;
        }

        // POST api/customers
        [HttpPost]
        [ActionName("AddCustomer")]
        public HttpResponseMessage AddCustomer(Customer customer)
        {
            if (ModelState.IsValid)
            {
                _db.Customers.Add(customer);
                _db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, customer);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = customer.Id }));
                return response;
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/customers/5
        [AcceptVerbs("POST")]
        [HttpPut]
        [ActionName("EditCustomer")]
        public HttpResponseMessage EditCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != customer.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            _db.Entry(customer).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE api/customers/5
        [AcceptVerbs("POST")]
        [HttpDelete]
        [ActionName("DeleteCustomer")]
        public HttpResponseMessage DeleteCustomer(int id)
        {
            var customer = _db.Customers.Find(id);
            if (customer == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            _db.Customers.Remove(customer);
            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, customer);
        }
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
