﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SignageManagement.Data.Models;
using SignageManagement.Data;

namespace SignageManagement.WebAPI.Controllers
{
     [Authorize]
    public class SerialNumberController : Controller
    {
        private SignageManagementContext db = new SignageManagementContext();

        //
        // GET: /SerialNumber/

        public ActionResult Index()
        {
            return View(db.SerialNumbers.Where(c => c.Active).Include(t => t.Customer).ToList());
        }

        //
        // GET: /SerialNumber/Details/5

        public ActionResult Details(int id = 0)
        {
            var serialnumber = db.SerialNumbers.Find(id);
            if (serialnumber == null)
            {
                return HttpNotFound();
            }
            return View(serialnumber);
        }

        //
        // GET: /SerialNumber/Create

        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName");
            return View();
        }

        //
        // POST: /SerialNumber/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CustomerId,HwSerialNumber,Type, LicenseExpiryDate,MaintExpiryDate")]SerialNumber serialnumber)
        {
            if (ModelState.IsValid)
            {
                if (db.SerialNumbers.Any(c => c.HwSerialNumber == serialnumber.HwSerialNumber.Trim()))
                {
                    ModelState.AddModelError("", "Serial # already exists!");
                    return View(serialnumber);
                }
                serialnumber.Active = true;
                serialnumber.DateAdded = serialnumber.DateLastModified = DateTime.Now;
                db.SerialNumbers.Add(serialnumber);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName");
            return View(serialnumber);
        }

        //
        // GET: /SerialNumber/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SerialNumber serialnumber = db.SerialNumbers.Find(id);
            if (serialnumber == null)
            {
                return HttpNotFound();
            }
            return View(serialnumber);
        }

        //
        // POST: /SerialNumber/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SerialNumber serialnumber)
        {
            if (ModelState.IsValid)
            {
                if (db.SerialNumbers.Any(c => c.HwSerialNumber == serialnumber.HwSerialNumber.Trim() && c.Id != serialnumber.Id))
                {
                    ModelState.AddModelError("", "Serial # already exists!");
                    return View(serialnumber);
                }
                serialnumber.DateLastModified = DateTime.Now;
                db.Entry(serialnumber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(serialnumber);
        }

        //
        // GET: /SerialNumber/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SerialNumber serialnumber = db.SerialNumbers.Find(id);
            if (serialnumber == null)
            {
                return HttpNotFound();
            }
            return View(serialnumber);
        }

        //
        // POST: /SerialNumber/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SerialNumber serialnumber = db.SerialNumbers.Find(id);
            serialnumber.Active = false;
            db.Entry(serialnumber).State = EntityState.Modified;

            //db.SerialNumbers.Remove(serialnumber);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}