﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Schema;
using SignageManagement.Data;
using SignageManagement.Data.Models;

namespace SignageManagement.WebAPI.Controllers
{
    public class SerialNumbersController : ApiController
    {
        private readonly SignageManagementContext _db = new SignageManagementContext();

        // GET api/serialNumbers
        [HttpGet]
        [ActionName("GetSerialNumbers")]
        public IEnumerable<SerialNumber> GetSerialNumbers()
        {
            return _db.SerialNumbers.AsEnumerable();
        }

        // GET api/serialNumbers/5
        [HttpGet]
        [ActionName("GetSerialNumber")]
        public SerialNumber GetSerialNumber(int id)
        {
            var serialNumber = _db.SerialNumbers.Find(id);
            if (serialNumber == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return serialNumber;
        }

        [HttpGet]
        [ActionName("ValidateSerialNumber")]
        public bool ValidateSerialNumber(string id)
        {
            var serialNumber = _db.SerialNumbers.FirstOrDefault(c => c.HwSerialNumber == id.Trim());
            if (serialNumber==null)
            return false;
            if (serialNumber.Type == "NUC")
            {
                if (DateTime.Now > serialNumber.MaintExpiryDate)
                {
                    return false;
                }
            }
            if (serialNumber.Type == "Stick")
            {
                if (DateTime.Now > serialNumber.LicenseExpiryDate)
                {
                    return false;
                }
            }
            return true;
        }

        // POST api/serialNumbers
        [HttpPost]
        [ActionName("AddSerialNumber")]
        public HttpResponseMessage AddSerialNumber(SerialNumber serialNumber)
        {
            if (ModelState.IsValid)
            {
                var customer = _db.Customers.Find(serialNumber.Customer.Id);
                serialNumber.Customer = customer;
                _db.SerialNumbers.Add(serialNumber);
                _db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, serialNumber);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = serialNumber.Id }));
                return response;
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/serialNumbers/5
        [AcceptVerbs("POST")]
        [HttpPut]
        [ActionName("EditSerialNumber")]
        public HttpResponseMessage EditSerialNumber(int id, SerialNumber serialNumber)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != serialNumber.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            //Manually updating FK
            var serialNumberExisting = _db.SerialNumbers.Include(c => c.Customer).Single(c => c.Id == serialNumber.Id);
            var customer = _db.Customers.Single(c => c.Id == serialNumber.Customer.Id);
            _db.Entry(serialNumberExisting).CurrentValues.SetValues(serialNumber);
            _db.Customers.Attach(customer);
            serialNumberExisting.Customer = customer;
            _db.Entry(serialNumberExisting).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE api/serialNumbers/5
        [AcceptVerbs("POST")]
        [HttpDelete]
        [ActionName("DeleteSerialNumber")]
        public HttpResponseMessage DeleteSerialNumber(int id)
        {
            var serialNumber = _db.SerialNumbers.Find(id);
            if (serialNumber == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            _db.SerialNumbers.Remove(serialNumber);
            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, serialNumber);
        }
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
