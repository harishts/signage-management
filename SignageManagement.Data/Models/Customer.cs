﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignageManagement.Data.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("Customer ID")]
        [Required(ErrorMessage = "Customer ID is Required")]
        public string CustomerId { get; set; }
        [DisplayName("Customer Name")]
        [Required(ErrorMessage = "Customer name is Required")]
        public string CustomerName { get; set; }
         [DisplayName("Phone #")]
        public string PhoneNumber { get; set; }
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        public bool Active { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime LastModified { get; set; }

    }
}
