﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignageManagement.Data.Models
{
    public class SerialNumber
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Customer name is Required")]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        [Required(ErrorMessage = "Serial Number is Required")]
        public string HwSerialNumber { get; set; }
        public string Type { get; set; }
        public bool Active { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LicenseExpiryDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MaintExpiryDate { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateLastModified { get; set; }
    }
}
