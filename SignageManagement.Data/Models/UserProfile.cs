﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SignageManagement.Data.Models
{
    public class UserProfile
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Username is Required")]
        [DisplayName("Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "First Name is Required")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is Required")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

    }
}
