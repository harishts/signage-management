﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignageManagement.Data.Models;

namespace SignageManagement.Data
{
    public class SignageManagementContext : DbContext, IDisposable
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<SerialNumber> SerialNumbers { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Membership> Membership { get; set; }
        public DbSet<Role> Roles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Membership>()
              .HasMany<Role>(r => r.Roles)
              .WithMany(u => u.Members)
              .Map(m =>
              {
                  m.ToTable("webpages_UsersInRoles");
                  m.MapLeftKey("UserId");
                  m.MapRightKey("RoleId");
              });
        }
    }
}
