﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using SignageManagement.Data.Models;

namespace SignageManagement.Data
{
    public class SignageManagementInitializer : DropCreateDatabaseIfModelChanges<SignageManagementContext>
    {
        protected override void Seed(SignageManagementContext context)
        {
            IList<Customer> customers = new List<Customer>();
            var customer = new Customer
            {
                Active = true,
                CustomerId = "TS1",
                CustomerName = "ThoughtStudio",
                Address = "Unit 35, 56 O'Riordan Street,Alexandria, NSW, 2015",
                PhoneNumber = "1300161803",
                DateAdded = DateTime.Now,
                LastModified = DateTime.Now
            };
            customers.Add(customer);
            foreach (var cus in customers)
                context.Customers.Add(cus);
            base.Seed(context);
        }
    }
}
